import urequests as http
import time
from machine import RTC

import config
from neopixel import Neopixel

# keep track of the day
day = 0

# store the forecasts in an array
forecast = []

# set the total amount of leds on the LED strip
total_leds = 41

# define the LED strip
strip = Neopixel(total_leds, 1, 0, "GRB")

# openweathermap.org variables
lat = config.LAT
lon = config.LON
owm_api_key = config.OWM_API_KEY

# show the current day as the first day
skip_today = True

# the speed of the day cycle
cycle_speed = 2

# show the weather animation or not
animated = True

# colors
white = (255, 255, 255)
orange = (255, 165, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
purple = (128, 0, 128)

# weather elements with the start index + the amount of leds + the color
sun = [0, 6, yellow]
halfSun = [6, 6, yellow]
cloud = [12, 6, white]
heavyRain = [24, 4, blue]
lightRain = [28, 4, blue]
drizzle = [32, 3, blue]
snow = [28, 4, white]

days = [35, 6, white]

# weather types, some weather elements are combined into one type
weather_icons = {
  "sun":     [sun],
  "cloudy":  [halfSun, cloud],
  "clouds":  [cloud],
  "drizzle": [halfSun, drizzle, cloud],
  "rainy":   [halfSun, lightRain, cloud],
  "rain":    [lightRain, cloud],
  "showers": [heavyRain, cloud],
  "snow":    [snow, cloud]
}


def getIcon(code, id):
    if code in ['01d', '01n']:
        # Sunny
        icon = 'sun'
    elif code in ['02d', '02n']:
        # Few clouds
        icon = 'cloudy'
    elif code in ['03d', '03n', '04d', '04n']:
        # Cloudy
        icon = 'clouds'
    elif code in ['09d', '09n']:
        # Showers
        icon = 'showers'
    elif code in ['10d', '10n', '11d', '11n']:
        if id == 500:
            icon = 'drizzle'
        elif id == 501:
            icon = 'rain'
        elif id in [502, 503, 504]:
            icon = 'showers'
        else:
            icon = 'showers'
    elif code in ['13d', '13n']:
        icon = 'snow'
    else:
        icon = code
    return icon


# reset all the leds
def reset_all():
    strip.clear()


def get_week_day():
    global day
    now = RTC().datetime()
    week_day = (now[3] + day + skip_today) % 7

    return week_day


# check if it's a weekend day
def is_weekend():
    week_day = get_week_day()
    return week_day == 5 or week_day == 6


# update the leds with the current weather
def set_leds(weather):

    # set the current day and check if it's a weekend day
    day_color = purple if is_weekend() else orange
    strip.set_pixel(days[0] + day, day_color)

    strip.show()

    for icon in weather:
        for i in range(icon[0], icon[0] + icon[1]):
            strip.set_pixel(i, icon[2])
            if animated:
                time.sleep(0.05)
                strip.show()

    if not animated:
        strip.show()


def update_weather():
    global day

    # convert the weekday to the name of the day for debugging
    # week_day = get_week_day()

    # days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    # print(days[week_day], ': ', forecast[day])

    set_leds(weather_icons[forecast[day]])


def start_day_cycle():
    global day

    # Schedule the function to run every 4 hours
    interval = 4 * 60 * 60  # Interval in seconds
    next_run = time.time() + interval

    print('start the day cycle and schedule the weather update')
    while True:
        reset_all()
        update_weather()
        time.sleep(cycle_speed)
        # increase the day with one, if we reach the end of the 6 days we start over
        day = (day + 1) % 6

        if time.time() >= next_run:
            get_weather()
            next_run += interval


def get_weather():
    global forecast
    print('get the weather from openweathermap.org')

    url = f"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={owm_api_key}&exclude=current,minutely,hourly,alerts"
    response = http.get(url)

    # parse the response to json
    weather_data = response.json()

    forecast = []

    # get the daily forecasts
    if weather_data.get('daily') and len(weather_data['daily']) > 0:
        # if we skip today's weather we remove the first item, else the last
        if skip_today is True:
            weather_data['daily'].pop(0)
        else:
            weather_data['daily'].pop()

        # we want to end up with a 6-day forecast, so we remove the last item
        weather_data['daily'].pop()

        for day in weather_data['daily']:
            forecast.append(getIcon(day['weather'][0]['icon'], day['weather'][0]['id']))

        print('The 6-day forecast is:')
        print(forecast)


if __name__ == "__main__":
    # Get the weather from openweathermap.org
    get_weather()

    # Start the day cycle
    start_day_cycle()

