import time
from neopixel import Neopixel

total_leds = 41

# colors
white = (255, 255, 255)
orange = (255, 165, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
purple = (128, 0, 128)

strip = Neopixel(total_leds, 1, 0, "GRB")

# weather elements
sun = [0, 6, yellow]
halfSun = [6, 6, yellow]
cloud = [12, 6, white]
heavyRain = [24, 4, blue]
lightRain = [28, 4, blue]
drizzle = [32, 3, blue]
snow = [28, 4, white]

days = [35, 6, white]

# weather leds

weather_icons = {
  "sun":     [sun],
  "cloudy":  [halfSun, cloud],
  "clouds":  [cloud],
  "drizzle": [halfSun, drizzle, cloud],
  "rainy":   [halfSun, lightRain, cloud],
  "rain":    [lightRain, cloud],
  "showers": [heavyRain, cloud],
  "snow":    [snow, cloud]
}

while True:
    print('running')
    for weather in weather_icons:
        print(weather)
        for icon in weather_icons[weather]:
            print(icon)
            print('-------')
            print(range(icon[0], icon[1]))
            for i in range(icon[0], icon[0] + icon[1]):
                print(i, icon[2])
                strip.set_pixel(i, icon[2])

            time.sleep(0.01)
            strip.show()
        time.sleep(1)
        strip.clear()